#include "widget.h"
#include "ui_widget.h"
#include "widget.h"
#include <QPixmap>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    QPixmap pix(":/img/zdj1.jpg");
    ui->label_4->setPixmap(pix.scaled(221,211));
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_toolButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void Widget::on_toolButton_2_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
}

void Widget::on_toolButton_3_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);
}

void Widget::on_toolButton_4_clicked()
{
    ui->stackedWidget->setCurrentIndex(3);
}



void Widget::on_pushButton_2_clicked()
{
    ui->stackedWidget->setCurrentIndex(4);
}
